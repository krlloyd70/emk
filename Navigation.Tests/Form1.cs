﻿namespace Navigation.Tests
{
    #region

    using System;
    using System.Windows.Forms;
    using EMK.Cartography;

    #endregion

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Graph g;
            Mapper.CreateMapFromNodeFile(@"C:\nodefile.nodemap", @"C:\nodefile.map", out g);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var m = new Mapper(@"C:\nodefile.nodemap");
            //add points
            for (int x = 0; x < 10; x++)
            {
                for (int y = 0; y < 10; y++)
                {
                    for (int z = 0; z < 10; z++)
                    {
                        m.AddNode(new Node(x, y, z), 2);
                    }
                }
            }
            m.Close();
        }
    }
}