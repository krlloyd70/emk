﻿/* A Simple Mapper Class by Streppel as Contribution for OwnedCore/MMOwned
 * The A-Star Class is from http://www.codeproject.com/KB/recipes/graphs_astar.aspx , my only modification to this was to make the Nodes and Arcs attributes List<T> instead of ArrayLists(improves the performance when generating the arcs later on)
 * I'm not too familiar with all the licenses around so i'll just write here
 * You are free to use it,share it,change it etc. If you change anything and think it would be good for others too to have this changed,please write it in the Thread of this Class on MMOwned.com
 * 
 */
namespace Navigation
{
    #region

    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Threading.Tasks;
    using EMK.Cartography;

    #endregion

    public class Mapper
    {
        private string _nodeFile = "";
        private List<Node> _nodes = new List<Node>();

        /// <summary>
        /// Initialised the Mapper
        /// <param name="nodeFile">The Node-File where to save the Nodes-List</param>
        /// </summary>
        public Mapper(string nodeFile)
        {
            if (!File.Exists(nodeFile))
            {
                using (Stream streamRead = File.Create(nodeFile))
                {
                    streamRead.Close();
                }
            }
            else
            {
                using (Stream streamRead = File.OpenRead(nodeFile))
                {
                    if (_nodes == null || _nodes.Count == 0)
                    {
                        throw new IOException("You can't serialize an empty List!");
                    }

                    var binaryRead = new BinaryFormatter();
                    _nodes = (List<Node>) binaryRead.Deserialize(streamRead);

                    streamRead.Close();
                }
            }
            _nodeFile = nodeFile;
        }

        /// <summary>
        /// Will add a Node to the current Map
        /// </summary>
        /// <param name="node">The node to add</param>
        /// <param name="distance">Distance to check whether where already is a point close to the one to be added,default is 2(meter,yard,whatever in this context)</param>
        /// <returns>True if the Node was addess successfully,False if not</returns>
        public bool AddNode(Node node, double distance = 2)
        {
            var hasNode = false;
            foreach (var n in _nodes.Where(n => Node.SquareEuclidianDistance(n, node) < distance))
            {
                hasNode = true;
            }
            if (!hasNode)
            {
                _nodes.Add(node);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Saves the current Map
        /// After doing this,the current object will not be useable anymore!
        /// </summary>
        public void Close()
        {
            using (Stream streamWrite = File.OpenWrite(_nodeFile))
            {
                var binaryWrite = new BinaryFormatter();
                binaryWrite.Serialize(streamWrite, _nodes);
                streamWrite.Close();
            }
            _nodes = null;
            _nodeFile = null;
        }

        /// <summary>
        /// This will create a Graph from a given NodeFile(you can create this with an object of this class).
        /// This Graph then can be used to navigate on it via the EMK.Cartography
        /// </summary>
        /// <param name="nodeFile">The Node-File previously created with an object of this class</param>
        /// <param name="mapFile">The Map-File,that will contain the whole Graph with Nodes,Arcs,etc</param>
        /// <param name="graph">The Graph that is saved in the Mapfile,but as an object instance for instant use</param>
        /// <param name="maxDistance">Maximum distance from one point to another. default is 3.5 to fit the 2 meter/yards from the default mapping settings</param>
        public static void CreateMapFromNodeFile(string nodeFile, string mapFile, out Graph graph,
                                                 double maxDistance = 3.5)
        {
            //Variables
            var g = new Graph();
            List<Node> nodeList;
            //End Variables

            //Load NodeMap from File
            using (Stream streamRead = File.OpenRead(nodeFile))
            {
                var binaryRead = new BinaryFormatter();
                g.Nodes.Clear();
                nodeList = (List<Node>) binaryRead.Deserialize(streamRead);
                streamRead.Close();
            }
            foreach (var n in nodeList)
            {
                g.Nodes.Add(n);
            }
            //End loading Map from File

            //Add an arc for each point that is within a distance of maxDistance from current Node
            Parallel.ForEach(g.Nodes, n =>
                {
                    foreach (var n2 in g.Nodes)
                    {
                        if (n == null || n2 == null || n == n2) continue;

                        var dist = Node.EuclidianDistance(n, n2);
                        if (dist <= maxDistance)
                        {
                            g.AddArc(n, n2, 1);
                        }
                    }
                });
            //End adding Arcs

            //save file
            Stream streamWrite = File.Create(mapFile);
            var binaryWrite = new BinaryFormatter();
            binaryWrite.Serialize(streamWrite, g);
            streamWrite.Close();
            //end saving file

            //return the completed Graph for further usage
            graph = g;
        }

        public static Graph LoadMap(string mapFile)
        {
            Stream streamRead = File.OpenRead(mapFile);
            var binaryRead = new BinaryFormatter();
            var graph = (Graph) binaryRead.Deserialize(streamRead);
            streamRead.Close();
            return graph;
        }
    }
}