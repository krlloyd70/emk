﻿namespace EMK.GraphFormer
{
    #region

    using System.Windows.Forms;

    #endregion

    public class FlickerFreePanel : Panel
    {
        public FlickerFreePanel()
        {
            SetStyle(ControlStyles.ResizeRedraw, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.DoubleBuffer, true);
            SetStyle(ControlStyles.UserPaint, true);
        }
    }
}